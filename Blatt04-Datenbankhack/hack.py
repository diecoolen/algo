from sys import stdin, stdout
from timeit import default_timer as timer

## functions ###################################################

# inserts 'key' into 'array' which has length 'm' and returns
# index of insertion linear probing
def insert_linear(array, m, key):
    for i in range(m):
        index = (key  + i) % m
        if array[index] == -1:
            array[index] = key
            return index
    return -1

# inserts 'key' into 'array' which has length 'm' and returns
# index of insertion using double hashing
def insert_double(array, m, key):
    for i in range(m):
        index = (key + i * (1 + (key % (m-1)))) % m
        if array[index] == -1:
            array[index] = key
            return index
    return -1

# function for reading strings with stdin
def get_string():
    return stdin.readline().strip()

# function for reading integers with stdin
def get_int():
    return int(get_string())

def get_ints():
    return list(map(int, get_string().split()))

## main programm ###############################################

t = get_int()

for i in range(t):
    mns = get_ints()
    m = mns[0]
    n = mns[1]
    s = mns[2]
    
    linear = [-1] * m       # initialize hash arrays
    double_hash = [-1] * m
    s_occured = False
    
    for j in range(n):
        current_key = get_int()
    
        if s_occured:
            continue         # output has already been done skip remaining input
        
        l = insert_linear(linear, m, current_key)       # get insertion indices
        d = insert_double(double_hash, m, current_key)
        
        if s == current_key:                # print indices and update s_occured
            s_occured = True
            stdout.write(str(l) + '\n')
            stdout.write(str(d) + '\n')
