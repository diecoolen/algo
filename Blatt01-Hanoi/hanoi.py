import queue

## function defining ###########################################
def hanoi(n): # never used but is the cool recursive function
    if n <= 1:
        return 1
    else:
        return 2 * hanoi(n - 1) + 1


def calcGame(n, k):
    if n == 1: # recursion base cases
        return [[1],[],[]]
    if n <= 0:
        return [[],[],[]]
    if k <= 0:
        return [[*range(n,0,-1)],[],[]]

    i = 0                    # maximum number of coins that can be moved as one tower 
    posMoves = 1             # number of moves to move a complete tower with coins i to 1
    nextPosMoves = 1         # just to check if still small enough
    while nextPosMoves <= k: # here the 'hanoi-sequence' is calculated
        posMoves = nextPosMoves
        nextPosMoves = 2 * posMoves + 1
        i += 1

    if(posMoves == k): # one tower of i coins can be moved completly, nothing else
        if i == n:
            return [[],[*range(n, 0, -1)],[]]
        elif n % 2 == i % 2:
            return [[*range(n,i,-1)],[*range(i, 0, -1)],[]]
        else:
            return [[*range(n,i,-1)],[],[*range(i, 0, -1)]]
    else: # one tower is moved completly, top remaining coin also, moved tower gets divided by remaining number of steps
        nextLevel = calcGame(i, k - posMoves - 1) # divide moved tower
        if n % 2 == (i+1) % 2: # see where to place moved tower
            towerA = [*range(n, i + 1, -1)] + nextLevel[2]
            towerB = [i+1] + nextLevel[1]
            towerC = [] + nextLevel[0]
        else:
            towerA = [*range(n, i + 1, -1)] + nextLevel[2]
            towerB = [] +  nextLevel[0]
            towerC = [i+1] + nextLevel[1]
        return [towerA, towerB, towerC]

def listToString(xs):
    if len(xs) == 0:
        return ""
    result = " " + str(xs[0])
    for i in range(1, len(xs)):
        result += "|" + str(xs[i])
    return result

## begin of runnig programm ########################################
t = int(input())

ns = queue.Queue(t);
ks = queue.Queue(t);

for i in range(t):
    nAndk = input().split()
    ns.put(int(nAndk[0]))
    ks.put(int(nAndk[1]))

for i in range(t):
    n = ns.get()
    k = ks.get()
    result = calcGame(n, k)

    for i in range(3):
        print(str(i+1) + ":" + listToString(result[i]))
