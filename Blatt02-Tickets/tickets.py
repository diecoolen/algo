from sys import stdin, stdout
import timeit

## function defining ###################################################

# exchange values in list by index
def exchange(heap, i1, i2):
    heap[i1] = heap[i1] + heap[i2]
    heap[i2] = heap[i1] - heap[i2]
    heap[i1] = heap[i1] - heap[i2]

# heapify a list starting at given index (default 1)
def heapify(heap, start=1):
    if len(heap) < 2 * start: # node is a leaf
        return
    if len(heap) == 2 * start: # node has one leaf
        if heap[start - 1] < heap[2 * start - 1]:
            exchange(heap, start - 1, 2 * start - 1)
        return
    
    greaterChild = (2 * start - 1) if heap[2 * start - 1] > heap[2 * start] else (2 * start)
    if heap[greaterChild] < heap[start - 1]:
        return
    
    exchange(heap, start - 1, greaterChild)
    heapify(heap, greaterChild + 1)
    
# 'sort' list to heap
def initHeap(heap):
    for i in range(len(heap)):
        heapify(heap, len(heap) - i)

# function for reading strings with stdin
def get_string():
    return stdin.readline().strip()

# function for reading integers with stdin
def get_int():
    return int(get_string())

## start of main prgramm ###############################################
c = get_int()
k = get_int()

i = 0

smallNums = []

for j in range(c):
    guessedNum = get_int()
    
    if guessedNum == 0:     # Martin is calling
        if i < k: stdout.write(str(-1) + "\n")
        else: stdout.write(str(smallNums[0]) + "\n")
    elif i < k:             # no k elements yet
        smallNums.append(guessedNum)
        i += 1
        if i == k:
            initHeap(smallNums)
    elif smallNums[0] > guessedNum:                   # already k elements
        smallNums[0] = guessedNum
        heapify(smallNums)
