from sys import stdin, stdout

## functions ###################################################

# function for reading strings with stdin
def get_string():
    return stdin.readline().strip()

# function for reading integers with stdin
def get_int():
    return int(get_string())

def get_ints():
    return list(map(int, get_string().split()))

## main programm ###############################################

[n, m] = get_ints()

xs = [0] * m
ys = [0] * m

for i in range(m):
    [xs[i], ys[i]] = get_ints()
    
xs.sort()
ys.sort()

if m % 2 == 0:
    upperIndex = int(m/2) - 1
    lowerIndex = int(m/2)
    
    xCoordinate = int((xs[upperIndex] + xs[lowerIndex]) / 2)
    yCoordinate = int((ys[upperIndex] + ys[lowerIndex]) / 2)
    
    stdout.write(str(xCoordinate) + ' ' + str(yCoordinate) + '\n')
else:
    stdout.write(str(xs[int(m/2)]) + ' ' + str(ys[int(m/2)]) + '\n')
