from sys import stdin, stdout
from math import inf
from queue import Queue

## functions ###################################################

# gets a shortest path from s to t and the minimum capacity on that path
def admonds_karp():
    res = [[],0]
    predecessor = [-1] * n
    capacity = [-1] * n
    
    predecessor[0] = 0
    Q = Queue(0)
    Q.put(0)
    while not Q.empty():
        u = Q.get()
        
        if u == t:
            pred = predecessor[t]
            res[0].append(pred)
            res[1] = capacity[t]
            while pred != 0:
                if res[1] > capacity[pred]:
                    res[1] = capacity[pred]
                pred = predecessor[pred]
                res[0].append(pred)
            res[0].reverse()
            res[0].append(t)
            return res
        
        for v in adj[u]:
            if predecessor[v[0]] == -1:
                predecessor[v[0]] = u
                capacity[v[0]] = v[1]
                Q.put(v[0])
    
    return None

# function for reading strings with stdin
def get_string():
    return stdin.readline().strip()

# function for reading integers with stdin
def get_int():
    return int(get_string())

def get_ints():
    return list(map(int, get_string().split()))

## main programm ###############################################

# read input
[n, m] = get_ints()
s = 0
t = n - 1

adj = [[] * 0 for i in range(n)] # init adjacency lists

for i in range(m):      # fill adjacency lists
    [a,b,c] = get_ints()
    adj[a].append([b,c])
    adj[b].append([a,c])

# ford-fulkerson-algo
max_flow = 0
path = admonds_karp()

while(path != None):
    max_flow += path[1]
    #TODO:
    # für alle Paare in path finde kanten von rechts nach links und von links nach recht
    # und subtrahiere/addiere path[1]
    for i in range(len(path[0]) - 1):
        left = path[0][i]
        right = path[0][i+1]
        
        for edge in adj[left]:
            if edge[0] == right:
                left_to_right = edge
                break
        
        right_to_left = None
        for edge in adj[right]:
            if edge[0] == left:
                right_to_left = edge
                break
        
        adj[left].remove(left_to_right)
        left_to_right[1] -= path[1]
        if left_to_right[1] > 0:
            adj[left].append(left_to_right)
        elif left_to_right[1] < 0:
            print('Error!!!!!!!!!!!!!!!!!!! Capacatiy less than zero')
        
        if right_to_left == None:
            adj[right].append([left,path[1]])
        else:
            adj[right].remove(right_to_left)
            right_to_left[1] += path[1]
            adj[right].append(right_to_left)
    
    #print('path found')
    #print(path)
    #path = None
    path = admonds_karp()

#print('No path')

stdout.write(str(max_flow) + '\n')
