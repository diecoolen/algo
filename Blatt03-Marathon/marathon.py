from sys import stdin, stdout
from timeit import default_timer as timer

## functions ###################################################
# returns the GaU as defined on the sheet but with the elements ordered as in start
def gau(start, finish, n):
    gau = 0
    for i in range(n):
        for j in range(i+1, n):
            if (start.index(finish[i]) > start.index(finish[j])):
                gau += 1
    return gau

# function for reading strings with stdin
def get_string():
    return stdin.readline().strip()

# function for reading integers with stdin
def get_int():
    return int(get_string())

def get_ints():
    return list(map(int, get_string().split()))

## main programm ###############################################

beginning = timer()

m = get_int()

for i in range(m):
    n = get_int()
    start = get_ints()
    finish = get_ints()
    min_overtakes = gau(start, finish, n)
    stdout.write(str(min_overtakes) + '\n')
    
end = timer()

print("Calculation Time: " + str(end-beginning))
