from itertools import permutations as p
import sys
from random import randint

def permutation(n):
    permutations = p(range(1,n+1))
    return permutations

m = int(sys.argv[1])

perm = list(permutation(m))

randi = randint(0, len(perm) - 1)
string = str(perm[randi][0])

for i in range(1,m):
    string += " " + str(perm[randi][i])

print(string)

randi = randint(0, len(perm) - 1)
string = str(perm[randi][0])

for i in range(1,m):
    string += " " + str(perm[randi][i])

print(string)
