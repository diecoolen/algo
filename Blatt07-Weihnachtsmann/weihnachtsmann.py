from sys import stdin, stdout
from math import inf
from timeit import default_timer as timer

# returns min of L but only regarding indices which are in W
def min_length(L,W):
    v = W[0]
    m = L[v]
    for i in W:
        if (L[i] > 0 and L[i] < m):
            v = i
            m = L[i]
    return v

# returns adjacents of v, if they are still in W
def adjacents(v,adj,W,n):
    res = []
    for i in range(n):
        if adj[v][i] > 0 and i in W:
            res.append(i)
    return res

# function for reading strings with stdin
def get_string():
    return stdin.readline().strip()

# function for reading integers with stdin
def get_int():
    return int(get_string())

def get_ints():
    return list(map(int, get_string().split()))

## main programm ###############################################
#tic = timer()
[n,m] = get_ints()

adj = [[-1] * n for i in range(n)] # init adjacency matrix
for i in range(n):
    adj[i][i] = 0

for i in range(m):      # fill adjacency matrix
    [u,v,w] = get_ints()
    adj[u][v] = w
    adj[v][u] = w

# dijkstra algorithm
L = [inf] * n        # list of lengths
W = list(range(n))   # list of nodes still to look at (all nodes at start)
L[0] = 0;
for i in range(n):
    v = min_length(L,W)
    W.remove(v)
    for w in adjacents(v,adj,W,n):
        if L[v] + adj[v][w] < L[w]:
            L[w] = L[v] + adj[v][w]

stdout.write(str(sum(L) * 2) + '\n')
#toc = timer()
#print('Dauer = ', (toc - tic) * 1000)
