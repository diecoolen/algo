from sys import stdin, stdout
from math import inf
from timeit import default_timer as timer

# returns adjacents of v, if they are still in W
def adjacents(v,adj,L,n):
    res = []
        tmp = []
    for i in adj[v]:
        tmp.append(i[0])
    for i in L:
        if i[0] in tmp:
            res.append()
    return res

# function for reading strings with stdin
def get_string():
    return stdin.readline().strip()

# function for reading integers with stdin
def get_int():
    return int(get_string())

def get_ints():
    return list(map(int, get_string().split()))

## main programm ###############################################
#tic = timer()
[n,m] = get_ints()

adj = [[] * 0 for i in range(n)] # init adjacency lists

#for i in range(n):
#    adj[i][i] = 0

for i in range(m):      # fill adjacency lists
    [u,v,w] = get_ints()
    adj[u].append([v,w])
    adj[v].append([u,w])

# dijkstra algorithm
L = [[i,inf] for i in range(n)]       # list of lengths

L[0][1] = 0

for i in range(n):
    v = L[0]
    # remove root from L
    L[0] = L.pop()
    # heapify stuff sachen TODO
    for w in adjacents(v[0],adj,L,n):
        if v[1] + w[1] < L[w[0]]:
            L[w[0]] = L[v] + w[1]

stdout.write(str(sum(L) * 2) + '\n')
#toc = timer()
#print('Dauer = ', (toc - tic) * 1000)
