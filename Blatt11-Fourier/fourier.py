from sys import stdin, stdout

## functions ###################################################

def fft(n,a,w,M):
    n2 = int(n/2)
    n2range = range(n2)
    if n == 1:
        return a
    u = fft(n/2,([a[i*2] for i in n2range]),w*w % M,M)
    v = fft(n/2,([a[i*2+1] for i in n2range]),w*w % M,M)
    c = 1
    y = [0] * int(n)
    for i in n2range:
        y[i] = (u[i] + c * v[i]) % M
        y[n2 + i] = (u[i] - c * v[i]) % M
        c = (c * w) % M
    return y
    

# function for reading strings with stdin
def get_string():
    return stdin.readline().strip()

# function for reading integers with stdin
def get_int():
    return int(get_string())

def get_ints():
    return list(map(int, get_string().split()))

## main programm ###############################################

# read input
n = get_int()
M = get_int()
w = get_int()

a = [0] * n;
for i in range(n):
    a[i] = get_int()

res = fft(n,a,w,M)

for i in range(n):
    stdout.write(str(res[i]) + '\n')

